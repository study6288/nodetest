var express = require('express');
var port = process.env.LISTEN_PORT || 8080;
var app = express();
var appName = process.env.APP_NAME || "[testapp]";

app.get('/', function(req, res){
    res.send("Hello World, welcome to " + appName);
});

app.listen(port);
console.log('Express started on port ' + port);
