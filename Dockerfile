FROM node:8-alpine

# push app to container
RUN mkdir -p /data/app && mkdir -p /data/shared
WORKDIR /data/app
COPY . /data/app


CMD [ "npm" ,  "run", "start"]
